submodules:
    git submodule update --init

math:
    ./void-mklive/mklive.sh -p 'sagemath' -k 'es' -l 'ca_ES.UTF-8'

jesus:
    #!/usr/bin/sh
    cd void-mklive
    ./build-x86-images.sh -- -k es -l es_ES.UTF-8 \
    -p "wayland gnome firefox firefox-i18n-es-ES chromium evince wireshark libreoffice libreoffice-i18n-es vscode typst gimp" \
    -S gdm -T "Void Linux Jesús"

